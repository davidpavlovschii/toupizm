from app.utils.tuple import find_second_from_first_in_tuples
from django.core.exceptions import ObjectDoesNotExist
from app.utils.time import time_format, datetime_humanize
from django.contrib.auth.models import User
from django.core.files.storage import FileSystemStorage
from django.db.models.signals import pre_delete, post_save
from django.dispatch.dispatcher import receiver
from django.core.validators import URLValidator
from django.conf import settings
from django.db import models
import datetime
import os
import json


class Picture(models.Model):
    picture = models.ImageField(null=True)
    def __str__(self):
        return "{0}".format(self.picture)

class Profile(models.Model):
    user = models.OneToOneField(User, null=True, on_delete=models.CASCADE)
    profile_pic = models.ForeignKey(Picture, blank=True, null=True, on_delete=models.SET_NULL)
    bio = models.TextField(max_length=4096, blank=True, null=True, default="Je suis un gopnik en herbe.")
    """
    def create_profile(sender, **kwargs):
        if kwargs['created']:
            Profile.objects.create(user = kwargs['instance'])
    post_save.connect(create_profile, sender=User)

    def delete(self, *args, **kwargs):
        if os.path.isfile(self.profile_pic.picture.path):
            os.remove(self.profile_pic.picture.path)
        super(Profile, self).delete(*args,**kwargs)

        https://youtu.be/_UU6Fe7lqIo?t=883
    """
    def __str__(self):
        return self.user.name

class Category(models.Model):
    name = models.TextField(max_length=4500)
    description = models.TextField(max_length=15000)

class Project(models.Model):
    name = models.TextField(max_length=4500)
    participants = models.ManyToManyField(Profile, blank=True)
    creator = models.ForeignKey(User, null=True, blank=True, on_delete=models.CASCADE)
    categorie = models.ForeignKey(Category, null=True, blank=True, on_delete=models.CASCADE)

class Comment(models.Model):
    author = models.ForeignKey(User, null=True, blank=True, on_delete=models.CASCADE)
    project = models.ForeignKey(Project, null=True, blank=True, on_delete=models.CASCADE)
    content = models.TextField(max_length=15000)

class Type_Competence(models.Model):
    nom = models.TextField(max_length=250, default="Useless turd")

class Competence(models.Model):
    name = models.TextField(max_length=250, default="Navigateur")
    type_comp = models.ForeignKey(Type_Competence, on_delete=models.CASCADE)  

class Qualification(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    competence = models.ForeignKey(Competence, on_delete=models.CASCADE)
    level = models.IntegerField(null=True, default = 0)





