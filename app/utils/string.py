def encode_str_from_key(content_str, key):
    """
        Encode a string from a key with 'vigenere cipher'
        Parameters:
                str<string>: The string to encode
                key<string>: The key to encode the string
    """
    encoded_chars = []
    for idx, c in enumerate(content_str):
        key_c = key[idx % len(key)]
        encoded_c = chr((ord(c) + ord(key_c)) % 256)
        encoded_chars.append(encoded_c)
    new_str = "".join(encoded_chars)
    return new_str