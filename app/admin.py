from django.contrib import admin
from app.models import Profile, Project, Picture, Type_Competence, Competence, Qualification, Category, Comment

admin.site.register(Comment)
admin.site.register(Profile)
admin.site.register(Project)
admin.site.register(Picture)
admin.site.register(Type_Competence)
admin.site.register(Competence)
admin.site.register(Qualification)
admin.site.register(Category)

# Register your models here.

